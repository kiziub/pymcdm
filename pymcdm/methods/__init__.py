from .topsis import TOPSIS
from .vikor import VIKOR
from .copras import COPRAS
from .promethee import PROMETHEE_II
from .comet import COMET
from .spotis import SPOTIS
